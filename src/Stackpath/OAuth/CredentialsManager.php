<?php
/**
 * @package stackpathclient
 * @author Dmitry Kruglov <dmitry.kruglov@coredna.com>
 * @copyright 2018 Core dna
 */

namespace Stackpath\OAuth;


class CredentialsManager
{
    const oauthEndpoint = 'https://gateway.stackpath.com/identity/v1/oauth2/token';
    const credentialsFile = '.credentials';

    private $clientId;
    private $clientSecret;
    private $token;

    /**
     * CredentialsManager constructor.
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct($clientId = null, $clientSecret = null)
    {
        // If clientId and clientSecret are passed explicitly, use them
        if (!empty($clientId) && !empty($clientSecret)) {

        }

        // TODO: Try cached token


        // TODO: Try cached credentials

        // TODO: fail with exception

    }

    public function renew()
    {

    }

    private function loadStoredCredentials()
    {
        $credentailsFile = self::credentialsFile;
        if (!file_exists($credentailsFile)) {
            return false;
        }
    }
}